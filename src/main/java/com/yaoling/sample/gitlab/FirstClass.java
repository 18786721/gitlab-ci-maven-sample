package com.yaoling.sample.gitlab;

import org.springframework.stereotype.Controller;

/**
 * Created by liangping on 2016-06-16.
 * <p>
 * 江苏摇铃网络科技有限公司，版权所有。
 * Copyright (C) 2015-2016 All Rights Reserved.
 */
@Controller
public class FirstClass {

    public void test(){
        System.out.println("  ");
    }

}
